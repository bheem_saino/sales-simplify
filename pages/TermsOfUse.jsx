import React from "react";
// import useDocumentTitle from "../../../../components/useDocumentTitle";

import FooterSeven from "../components/footer/FooterSeven";
import CopyRightThree from "../components/footer/CopyRightThree";
import ScrollspyNav from "react-scrollspy-nav";
import HeaderLandingAppointment from "../components/header/HeaderLandingAppointment";

const TermsOfUse = () => {
//   useDocumentTitle(
//     " Terms & Conditions || Deski-Saas & Software React Template"
//   );
  return (
    <div className="doc-container main-page-wrapper">
      <HeaderLandingAppointment checkScroll={true} navbar={true}/>
      {/* End Header */}

      {/* =====================================================
				Terms and Condition
			===================================================== */}

      <div className="terms_and_policy">
        <div className="container">
          <ScrollspyNav
            scrollTargetIds={["opt1", "opt2", "opt3", "opt4", "opt5", "opt6"]}
            activeNavClass="active"
            offset={170}
            scrollDuration="300"
          >
            <div className="row align-items-start">
              <div className="col-lg-4 sidenav-sticky">
                <ul className="nav nav-tabs">
                  <li className="nav-item">
                    <a className="nav-link active" href="#opt1">
                      1. Terms of Use
                    </a>
                  </li>
                  <li className="nav-item">
                    <a className="nav-link" href="#opt2">
                      2. Terms and conditions
                    </a>
                  </li>
                  <li className="nav-item">
                    <a className="nav-link" href="#opt3">
                      3. Privacy policy
                    </a>
                  </li>
                  <li className="nav-item">
                    <a className="nav-link" href="#opt4">
                      4. Cookie policy
                    </a>
                  </li>
                  <li className="nav-item">
                    <a className="nav-link" href="#opt5">
                      5. Third Party Links
                    </a>
                  </li>
                  <li className="nav-item">
                    <a className="nav-link" href="#opt6">
                      6. GDPR
                    </a>
                  </li>
                </ul>
              </div>

              <div className="col-lg-8">
                {/* Tab panes */}
                <div className="tab-content ">
                  <div id="opt1">
                    <h2 className="font-gilroy-bold">Terms of Use</h2>
                    <div className="update-date">LAST UPDATED: 6/26/2020</div>
                    <h3>1. Introduction</h3>
                    <p>
                    Sales Simplify LLC (’’Sales Simplify’’) provides its service (as defined below) to you through the Sales Simplify   website (the ’’site’’), subject to this terms of service agreement (’’tos’’). By accepting this tos or by accessing or using the service or site, you acknowledge that you have read, understood, and agree to be bound by this tos and our privacy policy (available at https://salessimplify.com/privacy-policy/), which is incorporated by reference into this terms of service.
                    </p>
                    

                  </div>
                  <div id="opt2">
                    <h2 className="font-gilroy-bold">Terms & Conditions</h2>
                    <div className="update-date">LAST UPDATED: 6/26/2020</div>
                    <h3>1. Introduction</h3>
                    <p>
                      This page, together with our Privacy Policy and the
                      Website Terms of Use tells you information about us and
                      the legal terms and conditions (the "Terms") on which we
                      make available to you our online ticket-selling solution
                      (the "Service").
                    </p>
                    <h3>2. Information about us</h3>
                    <p>
                      Tickettailor.com and buytickets.at and any of their
                      sub-domains is a site operated by Ticket Tailor, which is
                      the trading name of Zimma Ltd ("we", “our”, “us”). We are
                      registered in England and Wales under company number
                      07583551 and have our registered office at Unit 6, 14A
                      Andre St, London, E8 2AA. Our VAT number is GB184622203.
                    </p>
                    <p>
                      You can contact us by emailing{" "}
                      <a href="#">hi@deski.com</a>.
                    </p>
                  </div>
                  <div id="opt3">
                    <h2 className="font-gilroy-bold">Privacy policy</h2>
                    <div className="update-date">LAST UPDATED: 6/26/2020</div>
                    <h3>1. Introduction</h3>
                    <p>
                      We have taken a number of steps to comply with the GDPR.
                      For more details about our compliance and how we handle
                      your data please check our latest legal documents below:
                    </p>
                    <ul>
                      <li>
                        <a href="#">Privacy policy</a>
                      </li>
                      <li>
                        <a href="#">Terms and conditions</a>
                      </li>
                    </ul>
                    <h3>2. About us</h3>
                    <p>
                      When you use Ticket Tailor for selling tickets and
                      collecting data from your attendees you are the “data
                      controller” of all your attendees’ data. We are a “data
                      processor”, which means that we won’t do anything with
                      your attendees’ data other than what you need us to in
                      order to provide our service. We won’t be sending your
                      attendees any marketing emails, sharing their data with
                      anyone and if you want us to delete the data, just ask.
                      It’s your data. 100
                    </p>
                    <h3>3. Accessing our Website</h3>
                    <p>
                      The servers and databases that power Ticket Tailor are
                      located in the EU in Ireland. We inevitably use a small
                      number of third parties, some of which are outside of the
                      EU, to provide our ticketing service and we have contracts
                      with each of them that oblige them to comply with the
                      GDPR. You can find the full list of these third parties
                      and what we use them for here.
                    </p>
                  </div>
                  <div id="opt4">
                    <h2 className="font-gilroy-bold">Cookie policy</h2>
                    <div className="update-date">LAST UPDATED: 6/26/2020</div>
                    <h3>1. Introduction</h3>
                    <p>
                      We have taken a number of steps to comply with the GDPR.
                      For more details about our compliance and how we handle
                      your data please check our latest legal documents below:
                    </p>
                    <ul>
                      <li>
                        <a href="#">Privacy policy</a>
                      </li>
                      <li>
                        <a href="#">Terms and conditions</a>
                      </li>
                    </ul>
                    <h3>2. About us</h3>
                    <p>
                      When you use Ticket Tailor for selling tickets and
                      collecting data from your attendees you are the “data
                      controller” of all your attendees’ data. We are a “data
                      processor”, which means that we won’t do anything with
                      your attendees’ data other than what you need us to in
                      order to provide our service. We won’t be sending your
                      attendees any marketing emails, sharing their data with
                      anyone and if you want us to delete the data, just ask.
                      It’s your data. 100
                    </p>
                    <h3>3. Accessing our Website</h3>
                    <p>
                      The servers and databases that power Ticket Tailor are
                      located in the EU in Ireland. We inevitably use a small
                      number of third parties, some of which are outside of the
                      EU, to provide our ticketing service and we have contracts
                      with each of them that oblige them to comply with the
                      GDPR. You can find the full list of these third parties
                      and what we use them for here.
                    </p>
                  </div>
                  <div id="opt5">
                    <h2 className="font-gilroy-bold">Third Party Links</h2>
                    <div className="update-date">LAST UPDATED: 6/26/2020</div>
                    <h3>1. Introduction</h3>
                    <p>
                      We have taken a number of steps to comply with the GDPR.
                      For more details about our compliance and how we handle
                      your data please check our latest legal documents below:
                    </p>
                    <ul>
                      <li>
                        <a href="#">Privacy policy</a>
                      </li>
                      <li>
                        <a href="#">Terms and conditions</a>
                      </li>
                    </ul>
                    <h3>2. About us</h3>
                    <p>
                      When you use Ticket Tailor for selling tickets and
                      collecting data from your attendees you are the “data
                      controller” of all your attendees’ data. We are a “data
                      processor”, which means that we won’t do anything with
                      your attendees’ data other than what you need us to in
                      order to provide our service. We won’t be sending your
                      attendees any marketing emails, sharing their data with
                      anyone and if you want us to delete the data, just ask.
                      It’s your data. 100
                    </p>
                    <h3>3. Accessing our Website</h3>
                    <p>
                      The servers and databases that power Ticket Tailor are
                      located in the EU in Ireland. We inevitably use a small
                      number of third parties, some of which are outside of the
                      EU, to provide our ticketing service and we have contracts
                      with each of them that oblige them to comply with the
                      GDPR. You can find the full list of these third parties
                      and what we use them for here.
                    </p>
                  </div>
                  <div id="opt6">
                    <h2 className="font-gilroy-bold">GDPR</h2>
                    <div className="update-date">LAST UPDATED: 6/26/2020</div>
                    
                    <p>
                    The EU General Data Protection Regulation (GDPR) has set a new standard for how companies use and protect EU citizens’ data.</p><p> 

For that purpose, we considered it important to let you know about the new General Data Protection Regulation (GDPR) that took effect from May 25, 2018 for all businesses who maintain the data of EU residents and businesses.</p><p>

Businesses of all sizes are required to be in compliance with the GDPR as per new regulation, going forward. The GDPR is very broad in scope and can apply to businesses both in and outside of the EU.</p><p>

At Sales Simplify, we are committed to the security and privacy of our all customers, to ensure that we fulfill all obligations to maintain data integrity and provide transparency about how we process data.</p><p>

<p>1.All data is encrypted for security during transit and storage.</p>
<p>2.At our company, we perform background checks and provide data security training for all of our employees.</p>
<p>3.We strictly limit who has access to contact information, and those who do are only checking it to respond to your support requests and make sure everything is working as expected. Access is revoked quickly if there is no need.</p>
<p>4.We are committed to providing users with a safe experience, so we perform rigorous security testing for each new feature.</p>
<p>Sales Simplify is committed to maintaining the confidentiality of your personal data. In the event of a security incident, we will notify you as soon as possible and will provide reasonable assistance to help you mitigate or rectify the situation.</p>
</p>
<p>To get a detailed understanding, visit <a href="#">Privacy Policy</a> and <a href="#">Terms of Services</a>.</p>

                   
                   
                  </div>
                </div>
                {/*  /.tab-content */}
              </div>
            </div>
          </ScrollspyNav>
        </div>
      </div>

      <footer className="theme-footer-one pt-130 md-pt-70">
        <div className="top-footer">
          <div className="container">
            <FooterSeven />
          </div>
          {/* /.container */}
        </div>
        {/* /.top-footer */}

        <div className="container">
          <div className="bottom-footer-content">
            <CopyRightThree />
          </div>
          {/*  /.bottom-footer */}
        </div>
      </footer>
      {/* /.theme-footer-one */}
    </div>
  );
};

export default TermsOfUse;
