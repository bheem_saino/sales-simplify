import React,{useState,useEffect} from 'react'


//importing the necessary components
import DocEditorTabs from '../components/block-slider/DocEditorTabs'
import FancyFeatureNinteen from '../components/features/FancyFeatureNinteen'
import CopyRightThree from '../components/footer/CopyRightThree'
import FooterSeven from '../components/footer/FooterSeven'
import HeaderLandingAppointment from '../components/header/HeaderLandingAppointment'
import HeroBannerNine from '../components/hero-banner/HeroBannerNine'
import Social from '../components/social/Social'
import TestimonialSeven from '../components/testimonials/TestimonialSeven'

//importing AOS
import AOS from 'aos'
import ScrollToTop from "../components/ScrollToTop";
import "aos/dist/aos.css";
function MainPage() {
  useEffect(() => {
    AOS.init();
  }, []);
    const [click1, setClick1] = useState(false);
    const handleClick1 = () => setClick1(!click1);
    return (
      <div className="main-page-wrapper p0 font-gordita">
        <HeaderLandingAppointment navbar={false} />
      {/* End Header Landing Editor */}

      {/* 	=============================================
				Theme Hero Banner
			==============================================  */}
      <HeroBannerNine />

      {/* 		=============================================
				Fancy Feature Eighteen
			==============================================  */}
      <div className="fancy-feature-eighteen mt-130 md-mt-80" id="feature">
        <div className="container">
          <div className="row">
            <div className="col-xl-9 m-auto">
              <div
                className="title-style-eight text-center"
                data-aos="fade-up"
                data-aos-duration="1200"
              >
                <h2>
                  Build with <br /> 800+ Stunning Effects
                </h2>
                <p>
                  Rotate, merge, trim, split, crop and flip videos with a modern
                  & simple ui
                </p>
              </div>
            </div>
          </div>

          {/* Nav tabs */}
        </div>
        {/* End .container */}
        <DocEditorTabs />

        <img src="images/shape/175.svg" alt="" className="shapes shape-right" />
        <img src="images/shape/176.svg" alt="" className="shapes shape-left" />
      </div>
      {/* /.fancy-feature-eighteen */}

      {/* 	=============================================
				Fancy Feature Nineteen
			==============================================  */}
      {/**optional.............................. */}
      {/* <div className="fancy-feature-nineteen pt-180 md-pt-100" id="effect">
        <div className="container">
          <div className="row">
            <div
              className="col-xl-9 col-lg-11 m-auto"
              data-aos="fade-up"
              data-aos-duration="1200"
            >
              <div className="title-style-eight text-center mb-80 md-mb-40">
                <h2>Unleash Creativity with Pro Effects</h2>
                <p>
                  Explore advanced video editing features that only
                  professionals have access to!
                </p>
              </div>
            </div>
          </div>
          // end row
          <FancyFeatureNinteen />
        </div>
      </div>   */}
      {/* /.fancy-feature-nineteen */}

     {/**************************************************************************************************************** */ }
      
      {/* /.fancy-feature-twenty */}

      <div className="fancy-feature-twentyThree pt-170 md-pt-100" id="feature">
        <div className="container">
          <div
            className="title-style-nine text-center pb-180 md-pb-100"
            data-aos="fade-up"
            data-aos-duration="1200"
          >
            <h6>Our Features</h6>
            <h2>
              Deski is all About Easy Bookings, Efficient Service, &{" "}
              <span>
                Robust Interface <img src="images/shape/192.svg" alt="shape" />
              </span>
            </h2>
            <p>
              Our online booking software allows you to schedule your client’s
              appointments easily & efficiently.
            </p>
          </div>
          {/* End title */}

          <div className="block-style-twentyThree">
            <div className="row align-items-center">
              <div
                className="col-lg-6 order-lg-last ml-auto"
                data-aos="fade-left"
                data-aos-duration="1200"
              >
                <div className="screen-container ml-auto">
                  <div
                    className="oval-shape"
                    style={{ background: "#69FF9C" }}
                  ></div>
                  <div
                    className="oval-shape"
                    style={{ background: "#FFF170" }}
                  ></div>
                  <img
                    src="images/assets/screen_18.png"
                    alt=""
                    className="shapes shape-one"
                  />
                </div>
                {/* /.screen-container */}
              </div>
              <div
                className="col-lg-5 order-lg-first"
                data-aos="fade-right"
                data-aos-duration="1200"
              >
                <div className="text-wrapper">
                  <h6>One click away</h6>
                  <h3 className="title">Make your schedule Online easily.</h3>
                  <p>
                    From its medieval origins to the digital era, learn
                    everything there is to know about the ubiquitep lorem ipsum
                    passage.
                  </p>
                </div>
                {/*  /.text-wrapper */}
              </div>
            </div>
          </div>
          {/* /.block-style-twentyThree */}

          <div className="block-style-twentyThree">
            <div className="row">
              <div className="col-lg-6">
                <div
                  className="screen-container mr-auto"
                  data-aos="fade-right"
                  data-aos-duration="1200"
                >
                  <div
                    className="oval-shape"
                    style={{ background: "#FFDE69" }}
                  ></div>
                  <div
                    className="oval-shape"
                    style={{ background: "#FF77D9" }}
                  ></div>
                  <img
                    src="images/assets/screen_19.png"
                    alt="screen"
                    className="shapes shape-two"
                  />
                </div>
                {/* /.screen-container */}
              </div>
              <div
                className="col-lg-5 ml-auto"
                data-aos="fade-left"
                data-aos-duration="1200"
              >
                <div className="text-wrapper">
                  <h6>WORKFLOW MANAGEMENT</h6>
                  <h3 className="title">Automate reminders & follow-ups.</h3>
                  <p>
                    Calendly puts your entire meeting workflow on autopilot,
                    sending everything from reminder emails to thank you notes,
                    so you can focus on the work only you can do.
                  </p>
                </div>
                {/* /.text-wrapper */}
              </div>
            </div>
          </div>
          {/* /.block-style-twentyThree */}

          <div className="block-style-twentyThree">
            <div className="row">
              <div
                className="col-lg-6 order-lg-last ml-auto"
                data-aos="fade-left"
                data-aos-duration="1200"
              >
                <div className="screen-container ml-auto">
                  <div
                    className="oval-shape"
                    style={{ background: "#00F0FF" }}
                  ></div>
                  <div
                    className="oval-shape"
                    style={{ background: "#FC6BFF" }}
                  ></div>
                  <img
                    src="images/assets/screen_20.png"
                    alt="screen"
                    className="shapes shape-three"
                  />
                </div>
                {/* /.screen-container */}
              </div>
              <div
                className="col-lg-5 order-lg-first"
                data-aos="fade-right"
                data-aos-duration="1200"
              >
                <div className="text-wrapper">
                  <h6>ON-DEMAND SCHEDULING</h6>
                  <h3 className="title">
                    More connections and no cancellations.
                  </h3>
                  <p>
                    Prospects can schedule meetings in just a few clicks –
                    whenever the moment is right. And cancellations go down
                    because rescheduling is easy, fast, and on their terms.
                  </p>
                </div>
                {/* /.text-wrapper */}
              </div>
            </div>
          </div>
          {/* /.block-style-twentyThree */}
        </div>
      </div>
      {/************************************************************************************************************** */}
      {/*=====================================================
				Useable Tools
			===================================================== */}
      <div className="useable-tools-section-two bg-shape mb-200 md-mb-90">
        <div className="bg-wrapper">
          <div className="shapes shape-one"></div>
          <div className="shapes shape-two"></div>
          <div className="shapes shape-three"></div>
          <div className="shapes shape-four"></div>
          <div className="container">
            <div className="title-style-two text-center mb-70 md-mb-10">
              <div className="row">
                <div className="col-lg-10 col-md-11 m-auto">
                  <p>Integrates with your tools</p>
                  <h2>
                    Connect deski with the software you
                    <span>
                      use every
                      <img src="images/shape/line-shape-2.svg" alt="" />
                    </span>
                    day.
                  </h2>
                </div>
              </div>
            </div>
            {/*  /.title-style-two */}

            <div className="icon-wrapper">
              <Social />
            </div>
            {/* /.icon-wrapper */}
          </div>
          {/* /.container */}
        </div>
        {/* /.bg-wrapper */}
      </div>

      {/* =====================================================
				Client Feedback Slider Six
			===================================================== */}
      <div
        className="client-feedback-slider-six pt-170 md-pt-120"
        id="feedback"
      >
        <div className="inner-container">
          <div className="container">
            <div className="row">
              <div
                className="col-xl-7 col-lg-9 m-auto"
                data-aos="fade-up"
                data-aos-duration="1200"
              >
                <div className="title-style-eight text-center">
                  <h6>FEEDBACK</h6>
                  <h2>Trusted by millions of creators.</h2>
                </div>
              </div>
            </div>
            {/* End .row */}
          </div>
          {/* End .container */}
          <div className="clientSliderSix">
            <TestimonialSeven />
          </div>
        </div>
        {/* /.inner-container */}
      </div>
      {/* /.client-feedback-slider-six */}

      {/* 	=====================================================
				Fancy Short Banner Nine
			===================================================== */}
      <div className="fancy-short-banner-nine mt-170 md-mt-80" style={{ background:"#008fbd"}}>
        <div className="container">
          <div className="row">
            <div
              className="col-xl-8 col-lg-11 m-auto"
              data-aos="fade-up"
              data-aos-duration="1200"
            >
              <div className="title-style-seven text-center">
                <h2>200k+ Customers have our deski.Try it now!</h2>
                <p>Try it risk free — we don’t charge cancellation fees.</p>
              </div>
              {/*  /.title-style-six */}
            </div>
          </div>
          {/* End .row */}
          <div data-aos="fade-up" data-aos-duration="1200" data-aos-delay="150">
            <div
              className={
                click1 ? "dropdown download-btn show" : "dropdown download-btn"
              }
            >
              <button
                className="dropdown-toggle"
                onClick={handleClick1}
                type="button"
              >
                Get Deski app
              </button>
              <div className={click1 ? "dropdown-menu  show" : "dropdown-menu"}>
                <a 
                    className="dropdown-item d-flex align-items-center"
                    href="#"
                    onClick={handleClick1}
                >
                  <img src="images/icon/103.svg" alt="icon" />
                  <span>IOS & Android</span>
                </a>
                <a className="dropdown-item d-flex align-items-center" href="#" onClick={handleClick1}>
                  <img src="images/icon/104.svg" alt="icon" />
                  <span>Mac & Windows</span>
                </a>
              </div>
            </div>
          </div>
        </div>
        {/*  /.container */}
        <img
          src="images/shape/177.svg"
          alt="shape"
          className="shapes shape-one"
        />
        <img
          src="images/shape/178.svg"
          alt="shape"
          className="shapes shape-two"
        />
      </div>
      {/*  /.fancy-short-banner-nen */}

      {/* =====================================================
				Footer Style Seven
			===================================================== */}
      <footer className="theme-footer-seven mt-120 md-mt-100">
        <div className="lg-container">
          <div className="container">
            <FooterSeven />
          </div>

          <div className="container">
            <div className="bottom-footer">
              <CopyRightThree />
            </div>
          </div>
        </div>
        {/* /.lg-container */}
      </footer>
      {/* /.theme-footer-seven */}
    </div>
    )
}

export default MainPage
