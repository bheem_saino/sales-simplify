import React, { useState,useEffect } from "react";
import Scrollspy from "react-scrollspy";
// import { Link } from "react-router-dom";
import Link from 'next/link'

const HeaderLandingAppointment = (props) => {
  const [click, setClick] = useState(false);
  const handleClick = () => setClick(!click);
  const [navbar, setNavbar] = useState(false);

  //here this useEffect standardized the permanent header for PricingCustomerSupport

  useEffect(() => {
        setNavbar(props.navbar);
        window.addEventListener("scroll", changeBackground);
        return ;

  },[props])
  
  //since next.js is server side rendering thus it can't recognize the window event 

  //so therfore add the window.addEventListener("scroll", changeBackground); in useEffect hook

  const changeBackground = () => {
    if (props.checkScroll){
        setNavbar(true);
    }
    else{
        if (window.scrollY >= 90) {
            setNavbar(true);
          } else {
            setNavbar(false);
          }
    }
    
  };

  
  const Resources = [
    {
      name: "Blog",
      routerPath: "/blogs"
    },
    {
        name:"Help Center",
        routerPath: "https://help.salessimplify.com/en/"
    }
    ]

  return (
    <>
      {/* =============================================
				Theme Main Menu
			==============================================  */}
      <div
        className={
          navbar
            ? "theme-main-menu sticky-menu theme-menu-six bg-none fixed"
            : "theme-main-menu sticky-menu theme-menu-six bg-none"
        }
      >
        <div className="d-flex align-items-center">
          <div className="logo">
            <a href="/project-management">
              <img src="images/logo/deski_06.svg" alt="brand" />
            </a>
          </div>
          {/* End logo */}

          <div className="right-widget order-lg-3">
            <ul className="d-flex align-items-center">
              <li>
                <a
                  href="https://app.salessimplify.com/auth/login" target="_blank"
                  className="signIn-action d-flex align-items-center"
                >
                  <img src="images/icon/120.svg" alt="icon" />
                  <span>login</span>
                </a>
              </li>
              <li>
                <a href="https://app.salessimplify.com/auth/registration" target="_blank" className="signup-btn">
                  <span>Sign up</span>
                </a>
              </li>
            </ul>
          </div>
          {/* End .right-widget */}

          <nav
            id="mega-menu-holder"
            className="navbar navbar-expand-lg ml-lg-auto order-lg-2"
          >
            <div className="container nav-container">
              <div className="mob-header">
                <button className="toggler-menu" onClick={handleClick}>
                  <div className={click ? "active" : ""}>
                    <span></span>
                    <span></span>
                    <span></span>
                  </div>
                </button>
              </div>
              {/* End Header */}

              <div
                className="navbar-collapse collapse landing-menu-onepage"
                id="navbarSupportedContent"
              >
                <div className="d-lg-flex justify-content-between align-items-center">
                  <Scrollspy
                    className="navbar-nav  main-side-nav font-gordita"
                    items={["pricing","resources"]}
                    currentClassName="active"
                    offset={-90}
                  >
                    <li className="nav-item">
                      <a href="#pricing" className="nav-link">
                        {/* <Link to="/pricing-cs" className="nav-link">Pricing</Link> */}
                        <Link href="/PricingCustomerSupport">Pricing</Link>
                      </a>
                    </li>
                    
                    <li className="nav-item dropdown">
                        <a className="nav-link dropdown-toggle" href="#" data-toggle="dropdown">
                        Resources
                        </a>
                        <ul className="dropdown-menu">
                        {Resources.map((val, i) => (
                            <li key={i}>
                            <a href={val.routerPath} className="dropdown-item">
                                {val.name}
                            </a>
                            </li>
                        ))}
                        </ul>
                        {/* /.dropdown-menu */}
                    </li>
                   
                  </Scrollspy>
                </div>
              </div>
            </div>
          </nav>
        </div>
      </div>
      {/* /.theme-main-menu */}

      {/* Mobile Menu Start */}
      <div className={click ? "mobile-menu  menu-open" : "mobile-menu"}>
        <div className="logo order-md-1">
          {/**link to a tag */}
          <a href="#">
            <img src="images/logo/deski_06.svg" alt="brand" />
          </a>
          <div className="fix-icon text-dark" onClick={handleClick}>
            <img src="images/icon/close.svg" alt="icon" />
          </div>
          {/* Mobile Menu close icon */}
        </div>

        <Scrollspy
          className="navbar-nav"
          id="theme-menu-list"
          items={["feature", "service", "feedback"]}
          currentClassName="active"
          offset={-90}
        >
          <li className="nav-item">
            <a href="#feature" className="nav-link" onClick={handleClick}>
              Features
            </a>
          </li>
          <li className="nav-item">
            <a href="#service" className="nav-link" onClick={handleClick}>
              Services
            </a>
          </li>
          <li className="nav-item">
            <a href="#feedback" className="nav-link" onClick={handleClick}>
              Feedback
            </a>
          </li>
        </Scrollspy>
      </div>
      {/* Mobile Menu End */}
    </>
  );
};

export default HeaderLandingAppointment;
