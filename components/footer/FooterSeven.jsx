import React from "react";
// import { Link } from "react-router-dom";
import Link from "next/link"
import FormFooterSignup from "../form/FormFooterSignup";

const FooterSeven = () => {
  return (
    <div className="row ">
      <div
        className="col-xl-3 col-lg-2 mb-40"
        data-aos="fade-up"
        data-aos-duration="1200"
      >
        <div className="logo">
          <a href="index.html">
            <img src="images/logo/deski_06.svg" alt="logo" />
          </a>
        </div>
      </div>
      {/* End .col */}

      <div
        className="col-lg-2 col-md-6 mb-40"
        data-aos="fade-up"
        data-aos-duration="1200"
        data-aos-delay="100"
      >
        <h5 className="title">Links</h5>
        <ul className="footer-list">
          {/**here all a tags are links tags */}
          <li>
            <a href="/">Home</a>
          </li>
          <li>
            <a href="/pricing-cs">Pricing</a>
          </li>
          <li>
            <a href="/about-eo">About us</a>
          </li>
          <li>
            <a href="/features-customer-support">Careers</a>
          </li>
          <li>
            <a href="/solution-management">Features</a>
          </li>
          <li>
            <a href="/blog-v3">Blog</a>
          </li>
        </ul>
      </div>

      <div
        className="col-lg-3 col-md-6 mb-40"
        data-aos="fade-up"
        data-aos-duration="1200"
        data-aos-delay="150"
      >
        <h5 className="title">Legal</h5>
        <ul className="footer-list">
          {/**here a tags are link tags  */}
          <li>
            <Link href="/termsofuse">Terms of Use</Link>
          </li>
          <li>
            <a href="/terms-of-use">Terms & conditions</a>
          </li>
          <li>
            <a href="/terms-conditions">Privacy policy</a>
          </li>
          <li>
            <a href="/terms-conditions">Cookie policy</a>
          </li>
        </ul>
      </div>
      <div
        className="col-xl-4 col-lg-5 mb-40"
        data-aos="fade-up"
        data-aos-duration="1200"
        data-aos-delay="200"
      >
        <div className="newsletter">
          <h5 className="title">Newslettert</h5>
          <p>
            Join over <span>68,000</span> people getting our emails
          </p>

          <FormFooterSignup />

          <div className="info">
            We only send interesting and relevant emails.
          </div>
        </div>
        {/* /.newsletter */}
      </div>
    </div>
  );
};

export default FooterSeven;
